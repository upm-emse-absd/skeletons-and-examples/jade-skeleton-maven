package es.upm.emse.absd;

import es.upm.emse.absd.agents.HelloAgent;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HelloAgentTest {

    @Test void testHello() {
        String expected = "Hello!!! My name is John Doe. Nice to meet you :D";;
        assertEquals(expected, new HelloAgent().hello("John Doe"));
    }

}